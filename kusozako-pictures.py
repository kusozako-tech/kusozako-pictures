#!/usr/bin/env python3

# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_pictures.MainLoop import DeltaMainLoop


if __name__ == "__main__":
    DeltaMainLoop()
