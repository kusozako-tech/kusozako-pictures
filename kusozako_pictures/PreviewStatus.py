
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

UNLOADED = 0
PENDING = 1
LOADED = 2
FAILED = 3
