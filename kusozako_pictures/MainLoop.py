
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.main_loop.MainLoop import AlfaMainLoop
from . import APPLICATION_DATA
from .CommandLine import OPTIONS
from .GrandDispatch import DeltaGrandDispatch


class DeltaMainLoop(AlfaMainLoop):

    def _delta_call_loopback_main_window_ready(self, parent):
        DeltaGrandDispatch(parent)

    def _delta_info_command_line_options(self):
        return OPTIONS

    def _delta_info_application_data(self, key):
        return APPLICATION_DATA.get(key, None)

    def _delta_info_application_library_directory(self):
        return GLib.path_get_dirname(__file__)

    def _request_synchronization(self, message, user_data=None):
        print("delta > message uncaught", message, user_data)
