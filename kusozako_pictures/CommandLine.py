
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib


OPTIONS = [
    (
        GLib.OPTION_REMAINING,
        0,
        GLib.OptionFlags.NONE,
        GLib.OptionArg.STRING_ARRAY,
        "",
        "[Directory or Image File]"
    ),
    (
        "version",
        ord("V"),
        GLib.OptionFlags.NONE,
        GLib.OptionArg.NONE,
        _("show version"),
        None
    )
]
