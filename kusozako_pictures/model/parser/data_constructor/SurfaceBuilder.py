
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import cairo
from gi.repository import Gdk
from libkusozako3.Ux import Unit

ICON_WIDTH = Unit(16)
SIZE = Unit(16), Unit(16)
CONTENT_TYPE = cairo.CONTENT_COLOR_ALPHA
SURFACE = cairo.ImageSurface(cairo.Format.ARGB32, *SIZE)


class FoxtrotSurfaceBuilder:

    def _get_surface(self):
        surface = SURFACE.create_similar(CONTENT_TYPE, *SIZE)
        context = cairo.Context(surface)
        context.set_source_rgba(1, 1, 1, 0.25)
        context.rectangle(0, 0, *SIZE)
        context.fill()
        return surface

    def build(self):
        surface = self._get_surface()
        pixbuf = Gdk.pixbuf_get_from_surface(surface, 0, 0, *SIZE)
        return surface, pixbuf
