
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import PreviewStatus
from .SurfaceBuilder import FoxtrotSurfaceBuilder
from .Checksum import FoxtrotChecksum


class DeltaDataConstructor(DeltaEntity):

    def construct(self, user_data):
        gio_file, file_info, mime_type = user_data
        basename = gio_file.get_basename()
        fullpath = gio_file.get_path()
        md5_checksum = self._checksum.get_md5(fullpath)
        surface, pixbuf = self._surface_builder.build()
        row_data = (
            file_info,
            fullpath,
            basename,
            md5_checksum,
            GLib.utf8_collate_key_for_filename(basename, -1),
            surface,
            pixbuf,
            PreviewStatus.UNLOADED,
            mime_type
            )
        self._raise("delta > append", row_data)

    def __init__(self, parent):
        self._parent = parent
        self._surface_builder = FoxtrotSurfaceBuilder()
        self._checksum = FoxtrotChecksum()
