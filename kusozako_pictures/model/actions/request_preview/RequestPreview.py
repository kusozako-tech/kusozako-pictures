
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelSignals
from .PreviewSetter import FoxtrotPreviewSetter


class DeltaRequestPreview(DeltaEntity):

    def _timeout(self, end_path):
        model = self._enquiry("delta > model")
        try:
            tree_row = model[self._start_path]
            self._preview_setter.try_set_preview(tree_row)
            self._start_path.next()
            return self._start_path.compare(end_path) != 1
        except IndexError:
            return GLib.SOURCE_REMOVE

    def receive_transmission(self, user_data):
        signal, visible_range = user_data
        if signal != ModelSignals.REQUEST_PREVIEW:
            return
        self._start_path, end_path = visible_range
        GLib.timeout_add(1, self._timeout, end_path)

    def __init__(self, parent):
        self._parent = parent
        self._preview_setter = FoxtrotPreviewSetter()
        self._raise("delta > register model object", self)
