
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .DirectorySelected import DeltaDirectorySelected
from .request_preview.RequestPreview import DeltaRequestPreview
from .InitialPathActivator import DeltaInitialPathActivator
from .Initializer import DeltaInitializer


class EchoActions:

    def __init__(self, parent):
        DeltaDirectorySelected(parent)
        DeltaRequestPreview(parent)
        DeltaInitialPathActivator(parent)
        DeltaInitializer(parent)
