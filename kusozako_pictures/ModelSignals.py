
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

WIDGETS_REALIZED = 0            # None
DIRECTORY_SELECTED = 1          # str as path to directory
SET_INITIAL_PATH = 2            # str as fullpath
PARSING_FINISHED = 3            # None
REQUEST_PREVIEW = 4             # (TreePath, TreePath) as (Start, End)
PATH_ACTIVATED = 5              # tree_row at activated item
PIXBUF_LOADED = 6               # Pixbuf
DRAG_UPDATE = 7                 # (int, int) as (offset_x, offset_y)
DRAG_END = 8                    # None
ZOOM_BY_SCROLL = 9              # float as delta_y
ZOOM_IN = 10                    # None
ZOOM_OUT = 11                   # None
ZOOM_ORIGINAL = 12              # None
ZOOM_FIT_BEST = 13              # None
PIXBUF_ZOOMED = 14              # None
PIXBUF_ZOOMED_FIT_BEST = 15     # None
PIXBUF_ANIMATION_ITER = 16      # None
PIXBUF_ROTATE = 17              # int as angle
POSITION_FIT_BEST = 18          # Pixbuf
POSITION_ZOOM = 19              # (Pixbuf, Pixbuf) as (old_pixbuf, new_pixbuf)
POSITION_ROTATE = 20            # (Pixbuf, Pixbuf) as (old_pixbuf, new_pixbuf)
SOURCE_PIXBUF_ROTATE = 21       # int as angle
