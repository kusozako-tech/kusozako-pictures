
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.Transmitter import FoxtrotTransmitter
from .model.Model import DeltaModel
from .paned.Paned import DeltaPaned


class DeltaGrandDispatch(DeltaEntity):

    def _delta_info_model(self):
        return self._model

    def _delta_call_model_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_call_register_model_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        self._model = DeltaModel(self)
        DeltaPaned(self)
