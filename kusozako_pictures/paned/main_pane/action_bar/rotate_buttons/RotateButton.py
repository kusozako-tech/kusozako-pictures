
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelSignals


class AlfaRotateButton(Gtk.Button, DeltaEntity):

    ANGLE = "define signal name here."
    ICON_NAME = "define icon name here."
    TOOLTIP_TEXT = "define tooltip text here."

    def _on_clicked(self, button):
        param = ModelSignals.PIXBUF_ROTATE, self.ANGLE
        self._raise("delta > model signal", param)

    def __init__(self, parent):
        self._parent = parent
        image = Gtk.Image.new_from_icon_name(
            self.ICON_NAME,
            Gtk.IconSize.SMALL_TOOLBAR
            )
        Gtk.Button.__init__(
            self,
            image=image,
            relief=Gtk.ReliefStyle.NONE,
            tooltip_text=self.TOOLTIP_TEXT
            )
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
