
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from kusozako_pictures import ModelSignals


class DeltaLabel(Gtk.Label, DeltaEntity):

    def receive_transmission(self, user_data):
        signal, pixbuf = user_data
        if signal != ModelSignals.PIXBUF_LOADED:
            return
        text = "{} x {}".format(pixbuf.get_width(), pixbuf.get_height())
        self.set_label(text)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self, hexpand=True)
        self.set_size_request(-1, Unit(4))
        self._raise("delta > add to container", self)
        self._raise("delta > register model object", self)
