
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_pictures import ModelSignals
from .ZoomButton import AlfaZoomButton


class DeltaZoomOutButton(AlfaZoomButton):

    SIGNAL = ModelSignals.ZOOM_OUT
    ICON_NAME = "zoom-out-symbolic"
    TOOLTIP_TEXT = _("Zoom-out")
