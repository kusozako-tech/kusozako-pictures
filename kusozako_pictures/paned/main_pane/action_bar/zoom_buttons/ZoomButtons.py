
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ZoomFitBestButton import DeltaZoomFitBestButton
from .ZoomOriginalButton import DeltaZoomOriginalButton
from .ZoomInButton import DeltaZoomInButton
from .ZoomOutButton import DeltaZoomOutButton


class EchoZoomButtons:

    def __init__(self, parent):
        DeltaZoomFitBestButton(parent)
        DeltaZoomOriginalButton(parent)
        DeltaZoomInButton(parent)
        DeltaZoomOutButton(parent)
