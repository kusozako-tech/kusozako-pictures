
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_pictures import ModelSignals
from .ZoomButton import AlfaZoomButton


class DeltaZoomInButton(AlfaZoomButton):

    SIGNAL = ModelSignals.ZOOM_IN
    ICON_NAME = "zoom-in-symbolic"
    TOOLTIP_TEXT = _("Zoom-In")
