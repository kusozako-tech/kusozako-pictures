
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .open_directory_button.OpenDirectoryButton import DeltaOpenDirectoryButton
from .open_picture_button.OpenPictureButton import DeltaOpenPictureButton
from .Label import DeltaLabel
from .zoom_buttons.ZoomButtons import EchoZoomButtons
from .rotate_buttons.RotateButtons import EchoRotateButtons


class DeltaActionBar(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        DeltaOpenDirectoryButton(self)
        DeltaOpenPictureButton(self)
        DeltaLabel(self)
        EchoZoomButtons(self)
        EchoRotateButtons(self)
        self._raise("delta > add to container", self)
        self._raise("delta > css", (self, "primary-surface-color-class"))
