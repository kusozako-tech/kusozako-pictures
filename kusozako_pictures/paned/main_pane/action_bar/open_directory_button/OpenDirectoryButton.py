
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser.FileChooser import DeltaFileChooser
from kusozako_pictures import ModelSignals

FILE_CHOOSER_MODEL = {
    "type": "select-directory",
    "id": "open-directory",
    "title": _("Select Directory"),
    "directory": None,
    "read-write-only": True
    }


class DeltaOpenDirectoryButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        directory = DeltaFileChooser.run_for_model(self, FILE_CHOOSER_MODEL)
        if directory is None:
            return
        param = ModelSignals.DIRECTORY_SELECTED, directory
        self._raise("delta > model signal", param)

    def __init__(self, parent):
        self._parent = parent
        image = Gtk.Image.new_from_icon_name(
            "folder-open-symbolic",
            Gtk.IconSize.SMALL_TOOLBAR
            )
        Gtk.Button.__init__(
            self,
            relief=Gtk.ReliefStyle.NONE,
            tooltip_text=_("Open Directory"),
            image=image
            )
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
