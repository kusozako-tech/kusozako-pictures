
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelSignals


class DeltaSourcePixbufRotate(DeltaEntity):

    def _action(self, angle):
        old_pixbuf = self._enquiry("delta > source pixbuf")
        new_pixbuf = old_pixbuf.rotate_simple(angle)
        self._raise("delta > refresh pixbuf", new_pixbuf)

    def receive_transmission(self, user_data):
        signal, angle = user_data
        if signal == ModelSignals.SOURCE_PIXBUF_ROTATE:
            self._action(angle)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register model object", self)
