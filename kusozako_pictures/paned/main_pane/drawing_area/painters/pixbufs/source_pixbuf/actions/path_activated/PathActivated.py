
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelColumns
from kusozako_pictures import ModelSignals
from .StaticImage import DeltaStaticImage
from .Animation import DeltaAnimation


class DeltaPathActivated(DeltaEntity):

    def _action(self, tree_row):
        path = tree_row[ModelColumns.FULLPATH]
        mime_type = tree_row[ModelColumns.MIME_TYPE]
        self._static_image.try_load_image(path, mime_type)
        self._animation.try_start_animation(path, mime_type)
        self._raise("delta > application window title", path)

    def receive_transmission(self, user_data):
        signal, tree_row = user_data
        if signal == ModelSignals.PATH_ACTIVATED:
            self._action(tree_row)

    def __init__(self, parent):
        self._parent = parent
        self._animation = DeltaAnimation(self)
        self._static_image = DeltaStaticImage(self)
        self._raise("delta > register model object", self)
