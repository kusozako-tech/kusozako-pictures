

# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .path_activated.PathActivated import DeltaPathActivated
from .SourcePixbufRotate import DeltaSourcePixbufRotate


class EchoActions:

    def __init__(self, parent):
        DeltaPathActivated(parent)
        DeltaSourcePixbufRotate(parent)
