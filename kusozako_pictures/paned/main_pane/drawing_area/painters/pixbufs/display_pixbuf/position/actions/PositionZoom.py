
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelSignals


class DeltaPositionZoom(DeltaEntity):

    def _action(self, pixbufs):
        old_pixbuf, pixbuf = pixbufs
        gdk_rectangle = self._enquiry("delta > allocated size")
        origin_x, origin_y = self._enquiry("delta > origin")
        rectangle_half_width = gdk_rectangle.width/2
        rectangle_half_height = gdk_rectangle.height/2
        old_pixbuf_center_x = rectangle_half_width - origin_x
        old_pixbuf_center_y = rectangle_half_height - origin_y
        scale_x = old_pixbuf_center_x/old_pixbuf.get_width()
        scale_y = old_pixbuf_center_y/old_pixbuf.get_height()
        x = rectangle_half_width - pixbuf.get_width()*scale_x
        y = rectangle_half_height - pixbuf.get_height()*scale_y
        self._raise("delta > origin changed", (x, y))

    def receive_transmission(self, user_data):
        signal, pixbufs = user_data
        if signal == ModelSignals.POSITION_ZOOM:
            self._action(pixbufs)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register model object", self)
