
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelSignals


class DeltaPositionFitBest(DeltaEntity):

    def _action(self, pixbuf):
        gdk_rectangle = self._enquiry("delta > allocated size")
        x = (gdk_rectangle.width-pixbuf.get_width())/2
        y = (gdk_rectangle.height-pixbuf.get_height())/2
        self._raise("delta > origin changed", (x, y))

    def receive_transmission(self, user_data):
        signal, pixbuf = user_data
        if signal == ModelSignals.POSITION_FIT_BEST:
            self._action(pixbuf)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register model object", self)
