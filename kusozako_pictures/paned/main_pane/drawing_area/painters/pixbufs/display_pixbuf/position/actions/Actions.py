
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .PositionFitBest import DeltaPositionFitBest
from .PositionZoom import DeltaPositionZoom
from .PositionRotate import DeltaPositionRotate


class EchoActions:

    def __init__(self, parent):
        DeltaPositionFitBest(parent)
        DeltaPositionZoom(parent)
        DeltaPositionRotate(parent)
