
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .Drag import DeltaDrag
from .actions.Actions import EchoActions


class DeltaPosition(DeltaEntity):

    def get_position(self):
        return self._drag.get_total_offset(self._origin)

    def _delta_info_origin(self):
        return self._origin

    def _delta_call_origin_changed(self, new_origin):
        # x, y = new_origin
        self._origin = new_origin
        self._raise("delta > request queue draw")

    def _delta_call_drag_end(self, offset):
        new_x = self._origin[0]+offset[0]
        new_y = self._origin[1]+offset[1]
        self._origin = (new_x, new_y)

    def __init__(self, parent):
        self._parent = parent
        # coord_x, coord_y for allocated rectangle
        self._origin = (0, 0)
        self._drag = DeltaDrag(self)
        EchoActions(self)
