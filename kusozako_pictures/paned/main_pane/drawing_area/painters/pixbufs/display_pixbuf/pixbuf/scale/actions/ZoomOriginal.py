
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelSignals


class DeltaZoomOriginal(DeltaEntity):

    def _action(self):
        self._raise("delta > scale changed", 1)

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal == ModelSignals.ZOOM_ORIGINAL:
            self._action()

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register model object", self)
