
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelSignals


class DeltaZoomOut(DeltaEntity):

    def _action(self):
        current_scale = self._enquiry("delta > scale")
        new_scale = max(0.05, current_scale*0.95)
        self._raise("delta > scale changed", new_scale)

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal == ModelSignals.ZOOM_OUT:
            self._action()

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register model object", self)
