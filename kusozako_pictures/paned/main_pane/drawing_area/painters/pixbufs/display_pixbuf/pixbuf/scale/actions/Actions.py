
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ZoomByScroll import DeltaZoomByScroll
from .ZoomFitBest import DeltaZoomFitBest
from .ZoomOriginal import DeltaZoomOriginal
from .ZoomIn import DeltaZoomIn
from .ZoomOut import DeltaZoomOut


class EchoActions:

    def __init__(self, parent):
        DeltaZoomByScroll(parent)
        DeltaZoomFitBest(parent)
        DeltaZoomOriginal(parent)
        DeltaZoomIn(parent)
        DeltaZoomOut(parent)
