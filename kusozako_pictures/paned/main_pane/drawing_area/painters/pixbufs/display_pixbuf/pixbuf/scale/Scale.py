
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelSignals
from .actions.Actions import EchoActions


class DeltaScale(DeltaEntity):

    def get_scale(self):
        return self._scale

    def _delta_call_scale_changed(self, new_scale):
        self._scale = new_scale
        param = ModelSignals.PIXBUF_ZOOMED, None
        self._raise("delta > model signal", param)

    def _delta_call_scale_changed_fit_best(self, new_scale):
        self._scale = new_scale
        param = ModelSignals.PIXBUF_ZOOMED_FIT_BEST, None
        self._raise("delta > model signal", param)

    def __init__(self, parent):
        self._parent = parent
        self._scale = 1
        EchoActions(self)
