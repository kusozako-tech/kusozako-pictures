
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity


class DeltaBackground(DeltaEntity):

    def paint(self, cairo_context):
        gdk_rectangle = self._enquiry("delta > allocated size")
        cairo_context.set_source_rgba(1, 1, 1, 0.9)
        cairo_context.rectangle(
            0,
            0,
            gdk_rectangle.width,
            gdk_rectangle.height
            )
        cairo_context.fill()

    def __init__(self, parent):
        self._parent = parent
