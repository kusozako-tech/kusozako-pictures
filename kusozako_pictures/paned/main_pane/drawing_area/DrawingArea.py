
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelSignals
from .painters.Painters import EchoPainters
from .watchers.Watchers import EchoWatchers


class DeltaDrawingArea(Gtk.DrawingArea, DeltaEntity):

    def _on_draw(self, drawing_area, cairo_context):
        self._painters.paint(cairo_context)

    def _on_realize(self, drawing_area):
        param = ModelSignals.WIDGETS_REALIZED, None
        self._raise("delta > model signal", param)

    def _delta_info_drawing_area(self):
        return self

    def _delta_info_allocated_size(self):
        gdk_rectangle, _ = self.get_allocated_size()
        return gdk_rectangle

    def _delta_call_request_queue_draw(self):
        self.queue_draw()

    def __init__(self, parent):
        self._parent = parent
        Gtk.DrawingArea.__init__(self, vexpand=True)
        self._painters = EchoPainters(self)
        EchoWatchers(self)
        self.connect("draw", self._on_draw)
        self.connect("realize", self._on_realize)
        self._raise("delta > add to container", self)
