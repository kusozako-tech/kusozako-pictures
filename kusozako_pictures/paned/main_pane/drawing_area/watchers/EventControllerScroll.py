
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelSignals


class DeltaEventControllerScroll(DeltaEntity):

    def _timeout(self, id_, delta_y):
        if id_ == self._id:
            param = ModelSignals.ZOOM_BY_SCROLL, delta_y
            self._raise("delta > model signal", param)
        return GLib.SOURCE_REMOVE

    def _on_scroll(self, event_controller_scroll, delta_x, delta_y):
        self._id += 1
        id_ = self._id
        GLib.timeout_add(10, self._timeout, id_, delta_y)

    def _on_realize(self, drawing_area):
        self._event_controller_scroll = Gtk.EventControllerScroll.new(
            drawing_area,
            Gtk.EventControllerScrollFlags.VERTICAL
            )
        self._event_controller_scroll.connect("scroll", self._on_scroll)

    def __init__(self, parent):
        self._parent = parent
        self._id = 0
        drawing_area = self._enquiry("delta > drawing area")
        drawing_area.connect("realize", self._on_realize)
