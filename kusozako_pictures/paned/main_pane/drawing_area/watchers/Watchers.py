
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .GestureDrag import DeltaGestureDrag
from .EventControllerScroll import DeltaEventControllerScroll


class EchoWatchers:

    def __init__(self, parent):
        DeltaGestureDrag(parent)
        DeltaEventControllerScroll(parent)
