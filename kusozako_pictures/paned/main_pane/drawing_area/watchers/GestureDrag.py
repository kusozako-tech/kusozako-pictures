
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelSignals


class DeltaGestureDrag(DeltaEntity):

    def _on_drag_end(self, gesture_drag, offset_x, offset_y):
        param = ModelSignals.DRAG_END, None
        self._raise("delta > model signal", param)

    def _on_drag_update(self, gesture_drag, offset_x, offset_y):
        param = ModelSignals.DRAG_UPDATE, (offset_x, offset_y)
        self._raise("delta > model signal", param)

    def _on_realize(self, drawing_area):
        self._gesture_drag = Gtk.GestureDrag.new(drawing_area)
        self._gesture_drag.connect("drag-update", self._on_drag_update)
        self._gesture_drag.connect("drag-end", self._on_drag_end)

    def __init__(self, parent):
        self._parent = parent
        drawing_area = self._enquiry("delta > drawing area")
        drawing_area.connect("realize", self._on_realize)
