
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelColumns
from .actions.Actions import EchoActions
from .watchers.Watchers import EchoWatchers


class DeltaIconView(Gtk.IconView, DeltaEntity):

    def _delta_info_icon_view(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow()
        Gtk.IconView.__init__(
            self,
            model=self._enquiry("delta > model"),
            tooltip_column=ModelColumns.BASENAME,
            pixbuf_column=ModelColumns.THUMBNAIL
            )
        self.set_activate_on_single_click(True)
        scrolled_window.add(self)
        EchoActions(self)
        EchoWatchers(self)
        self._raise("delta > add to container", scrolled_window)
        user_data = scrolled_window, "secondary-theme-color-class"
        self._raise("delta > css", user_data)
