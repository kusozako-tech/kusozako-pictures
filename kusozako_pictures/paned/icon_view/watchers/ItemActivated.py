
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelSignals


class DeltaItemActivated(DeltaEntity):

    def _on_item_activated(self, icon_view, tree_path):
        model = icon_view.get_model()
        tree_row = model[tree_path]
        param = ModelSignals.PATH_ACTIVATED, tree_row
        self._raise("delta > model signal", param)

    def __init__(self, parent):
        self._parent = parent
        icon_view = self._enquiry("delta > icon view")
        icon_view.connect("item-activated", self._on_item_activated)
