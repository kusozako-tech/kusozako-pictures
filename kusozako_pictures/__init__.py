
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import gettext
import locale

GROUP_ID = "com.gitlab.kusozako-tech"
VERSION = "2022.10.14"
APPLICATION_NAME = "kusozako-pictures"
APPLICATION_ID = "{}.{}".format(GROUP_ID, APPLICATION_NAME)

locale.setlocale(locale.LC_ALL, None)
gettext.install(
    APPLICATION_NAME,
    "/usr/share/locale",
    names=('gettext', 'ngettext')
    )

LONG_DESCRIPTION = _("""Image viewer for kusozako project..
This software is licencced under GPL version 3 or any later version.""")

APPLICATION_DATA = {
    "name": APPLICATION_NAME,
    "id": APPLICATION_ID,
    "icon-name": APPLICATION_ID,
    "mime-type": ("inode/directory", "image/*"),
    "version": VERSION,
    "short description": "Image viewer.",
    "long-description": LONG_DESCRIPTION
    }
