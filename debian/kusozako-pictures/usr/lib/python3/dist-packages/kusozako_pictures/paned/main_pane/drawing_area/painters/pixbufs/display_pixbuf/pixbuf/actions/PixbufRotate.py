
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelSignals


class DeltaPixbufRotate(DeltaEntity):

    def _action(self, angle):
        source_pixbuf = self._enquiry("delta > source pixbuf")
        if source_pixbuf is None:
            return
        old_pixbuf = self._enquiry("delta > pixbuf")
        new_pixbuf = old_pixbuf.rotate_simple(angle)
        self._raise("delta > pixbuf", new_pixbuf.copy())
        signal_param = old_pixbuf.copy(), new_pixbuf.copy()
        param = ModelSignals.POSITION_ROTATE, signal_param
        self._raise("delta > model signal", param)
        param = ModelSignals.SOURCE_PIXBUF_ROTATE, angle
        self._raise("delta > model signal", param)

    def receive_transmission(self, user_data):
        signal, angle = user_data
        if signal == ModelSignals.PIXBUF_ROTATE:
            self._action(angle)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register model object", self)
