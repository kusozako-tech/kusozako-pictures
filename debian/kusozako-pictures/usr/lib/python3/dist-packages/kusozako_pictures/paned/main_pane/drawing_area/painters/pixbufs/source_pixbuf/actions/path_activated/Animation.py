# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from gi.repository import GdkPixbuf
from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelSignals


class DeltaAnimation(DeltaEntity):

    def _refresh_pixbuf(self, iter_, path):
        pixbuf = iter_.get_pixbuf()
        delay = iter_.get_delay_time()
        self._raise("delta > refresh pixbuf", pixbuf.copy())
        if self._resize_flag:
            self._resize_flag = False
            param = ModelSignals.ZOOM_FIT_BEST, None
        else:
            param = ModelSignals.PIXBUF_ANIMATION_ITER, None
        self._raise("delta > model signal", param)
        GLib.timeout_add(delay, self._timeout, iter_, path)

    def _timeout(self, iter_, path):
        if iter_.advance() and path == self._path:
            self._refresh_pixbuf(iter_, path)
        return GLib.SOURCE_REMOVE

    def try_start_animation(self, path, mime_type):
        self._path = path
        if mime_type != "image/gif":
            return
        self._resize_flag = True
        animation = GdkPixbuf.PixbufAnimation.new_from_file(path)
        iter_ = animation.get_iter()
        self._refresh_pixbuf(iter_, path)

    def __init__(self, parent):
        self._parent = parent
        self._path = None
        self._resize_flag = False
