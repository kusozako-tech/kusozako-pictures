
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GdkPixbuf
from .RotateButton import AlfaRotateButton


class DeltaRotateLeft(AlfaRotateButton):

    ANGLE = GdkPixbuf.PixbufRotation.COUNTERCLOCKWISE
    ICON_NAME = "object-rotate-left-symbolic"
    TOOLTIP_TEXT = _("Rotate Left")
