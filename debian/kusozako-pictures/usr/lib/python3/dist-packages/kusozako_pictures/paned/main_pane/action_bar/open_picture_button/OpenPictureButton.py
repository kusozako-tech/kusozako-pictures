
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gio
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser.FileChooser import DeltaFileChooser
from kusozako_pictures import ModelSignals

FILE_CHOOSER_MODEL = {
    "type": "select-file",
    "id": "open-picture",
    "title": _("Select Picture"),
    "directory": None,
    "read-write-only": True,
    "filters": ("image/*")
    }


class DeltaOpenPictureButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        path = DeltaFileChooser.run_for_model(self, FILE_CHOOSER_MODEL)
        if path is None:
            return
        param = ModelSignals.SET_INITIAL_PATH, path
        self._raise("delta > model signal", param)
        gio_file = Gio.File.new_for_path(path)
        directory_gio_file = gio_file.get_parent()
        directory = directory_gio_file.get_path()
        param = ModelSignals.DIRECTORY_SELECTED, directory
        self._raise("delta > model signal", param)

    def __init__(self, parent):
        self._parent = parent
        image_param = "image-x-generic-symbolic", Gtk.IconSize.SMALL_TOOLBAR
        Gtk.Button.__init__(
            self,
            relief=Gtk.ReliefStyle.NONE,
            tooltip_text=_("Open Picture"),
            image=Gtk.Image.new_from_icon_name(*image_param)
            )
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
