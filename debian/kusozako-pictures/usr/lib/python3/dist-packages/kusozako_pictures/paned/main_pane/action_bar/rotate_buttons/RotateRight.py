
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GdkPixbuf
from .RotateButton import AlfaRotateButton


class DeltaRotateRight(AlfaRotateButton):

    ANGLE = GdkPixbuf.PixbufRotation.CLOCKWISE
    ICON_NAME = "object-rotate-right-symbolic"
    TOOLTIP_TEXT = _("Rotate Left")
