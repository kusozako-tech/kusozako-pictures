
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .source_pixbuf.SourcePixbuf import DeltaSourcePixbuf
from .display_pixbuf.DisplayPixbuf import DeltaDisplayPixbuf


class DeltaPixbufs(DeltaEntity):

    def paint(self, cairo_context):
        self._display_pixbuf.paint(cairo_context)

    def _delta_info_source_pixbuf(self):
        return self._source_pixbuf.pixbuf

    def __init__(self, parent):
        self._parent = parent
        self._source_pixbuf = DeltaSourcePixbuf(self)
        self._display_pixbuf = DeltaDisplayPixbuf(self)
