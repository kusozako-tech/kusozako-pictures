
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ModelLoaded import DeltaModelLoaded


class EchoActions:

    def __init__(self, parent):
        DeltaModelLoaded(parent)
