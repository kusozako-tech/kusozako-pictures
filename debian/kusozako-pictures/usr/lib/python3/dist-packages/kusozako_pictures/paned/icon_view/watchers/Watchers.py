
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .VAdjustment import DeltaVAdjustment
from .ItemActivated import DeltaItemActivated


class EchoWatchers:

    def __init__(self, parent):
        DeltaVAdjustment(parent)
        DeltaItemActivated(parent)
