
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .icon_view.IconView import DeltaIconView
from .main_pane.MainPane import DeltaMainPane


class DeltaPaned(Gtk.Paned, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Paned.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            wide_handle=True
            )
        self.set_position(150)
        DeltaIconView(self)
        DeltaMainPane(self)
        self._raise("delta > add to container", self)
