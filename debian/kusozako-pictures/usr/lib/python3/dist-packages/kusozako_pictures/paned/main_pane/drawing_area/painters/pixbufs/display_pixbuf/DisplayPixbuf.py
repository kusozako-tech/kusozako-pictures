
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from libkusozako3.Entity import DeltaEntity
from .pixbuf.Pixbuf import DeltaPixbuf
from .position.Position import DeltaPosition


class DeltaDisplayPixbuf(DeltaEntity):

    def paint(self, cairo_context):
        pixbuf = self._pixbuf.get_pixbuf()
        if pixbuf is None:
            return
        x, y = self._position.get_position()
        Gdk.cairo_set_source_pixbuf(cairo_context, pixbuf, x, y)
        cairo_context.paint()

    def __init__(self, parent):
        self._parent = parent
        self._pixbuf = DeltaPixbuf(self)
        self._position = DeltaPosition(self)
