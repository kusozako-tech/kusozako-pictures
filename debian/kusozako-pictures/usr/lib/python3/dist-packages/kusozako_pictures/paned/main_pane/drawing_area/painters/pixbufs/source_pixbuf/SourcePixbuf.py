
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .actions.Actions import EchoActions


class DeltaSourcePixbuf(DeltaEntity):

    def _delta_call_refresh_pixbuf(self, pixbuf):
        self._pixbuf = pixbuf.copy()

    def __init__(self, parent):
        self._parent = parent
        self._pixbuf = None
        EchoActions(self)

    @property
    def pixbuf(self):
        return self._pixbuf
