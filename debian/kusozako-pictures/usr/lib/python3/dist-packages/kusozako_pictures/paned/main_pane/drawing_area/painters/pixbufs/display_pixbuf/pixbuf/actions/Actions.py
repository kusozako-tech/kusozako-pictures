
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .PixbufZoomed import DeltaPixbufZoomed
from .PixbufZoomedFitBest import DeltaPixbufZoomedFitBest
from .PixbufAnimationIter import DeltaPixbufAnimationIter
from .PixbufRotate import DeltaPixbufRotate


class EchoActions:

    def __init__(self, parent):
        DeltaPixbufZoomed(parent)
        DeltaPixbufZoomedFitBest(parent)
        DeltaPixbufAnimationIter(parent)
        DeltaPixbufRotate(parent)
