
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .scale.Scale import DeltaScale
from .actions.Actions import EchoActions


class DeltaPixbuf(DeltaEntity):

    def _delta_info_pixbuf(self):
        return self._pixbuf

    def _delta_call_pixbuf(self, pixbuf):
        self._pixbuf = pixbuf

    def _delta_info_scale(self):
        return self._scale.get_scale()

    def get_pixbuf(self):
        return self._pixbuf

    def __init__(self, parent):
        self._parent = parent
        self._pixbuf = None
        self._scale = DeltaScale(self)
        EchoActions(self)
