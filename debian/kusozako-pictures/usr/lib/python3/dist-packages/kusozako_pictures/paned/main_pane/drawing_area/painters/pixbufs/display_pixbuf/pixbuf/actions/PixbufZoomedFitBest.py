
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GdkPixbuf
from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelSignals

INTERP_TYPE = GdkPixbuf.InterpType.BILINEAR


class DeltaPixbufZoomedFitBest(DeltaEntity):

    def _action(self):
        source_pixbuf = self._enquiry("delta > source pixbuf")
        scale = self._enquiry("delta > scale")
        width = source_pixbuf.get_width()*scale
        height = source_pixbuf.get_height()*scale
        pixbuf = source_pixbuf.scale_simple(width, height, INTERP_TYPE)
        self._raise("delta > pixbuf", pixbuf)
        param = ModelSignals.POSITION_FIT_BEST, pixbuf.copy()
        self._raise("delta > model signal", param)

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal == ModelSignals.PIXBUF_ZOOMED_FIT_BEST:
            self._action()

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register model object", self)
