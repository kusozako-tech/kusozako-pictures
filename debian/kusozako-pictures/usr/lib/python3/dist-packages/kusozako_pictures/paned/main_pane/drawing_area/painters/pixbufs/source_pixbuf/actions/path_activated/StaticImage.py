
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GdkPixbuf
from libkusozako3.Entity import DeltaEntity
from libkusozako3 import WebP
from kusozako_pictures import ModelSignals


class DeltaStaticImage(DeltaEntity):

    def _try_get_pixbuf(self, path, mime_type):
        if mime_type == "image/webp":
            return WebP.to_pixbuf(path)
        elif mime_type != "image/gif":
            return GdkPixbuf.Pixbuf.new_from_file(path)
        return None

    def try_load_image(self, path, mime_type):
        pixbuf = self._try_get_pixbuf(path, mime_type)
        if pixbuf is None:
            return
        param = ModelSignals.PIXBUF_LOADED, pixbuf
        self._raise("delta > model signal", param)
        self._raise("delta > refresh pixbuf", pixbuf)
        param = ModelSignals.ZOOM_FIT_BEST, None
        self._raise("delta > model signal", param)

    def __init__(self, parent):
        self._parent = parent
