
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Background import DeltaBackground
from .pixbufs.Pixbufs import DeltaPixbufs


class EchoPainters:

    def paint(self, cairo_context):
        self._background.paint(cairo_context)
        self._pixbufs.paint(cairo_context)

    def __init__(self, parent):
        self._background = DeltaBackground(parent)
        self._pixbufs = DeltaPixbufs(parent)
