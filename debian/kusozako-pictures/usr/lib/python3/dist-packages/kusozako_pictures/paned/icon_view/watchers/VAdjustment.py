
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelSignals

DELAY = 50


class DeltaVAdjustment(DeltaEntity):

    def _timeout(self, visible_range, id_):
        if self._id == id_:
            start, end = visible_range
            param = ModelSignals.REQUEST_PREVIEW, visible_range
            self._raise("delta > model signal", param)
        return GLib.SOURCE_REMOVE

    def _on_value_changed(self, adjustment, icon_view):
        visible_range = icon_view.get_visible_range()
        if visible_range is None:
            return
        self._id += 1
        id_ = self._id
        GLib.timeout_add(DELAY, self._timeout, visible_range, id_)

    def __init__(self, parent):
        self._parent = parent
        self._id = 0
        icon_view = self._enquiry("delta > icon view")
        vadjustment = icon_view.get_vadjustment()
        vadjustment.connect("value-changed", self._on_value_changed, icon_view)
