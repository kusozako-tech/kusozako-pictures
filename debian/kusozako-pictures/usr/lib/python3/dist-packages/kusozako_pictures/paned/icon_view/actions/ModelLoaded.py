
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelSignals
from kusozako_pictures import ModelColumns


class DeltaModelLoaded(DeltaEntity):

    def _ensure_visble(self, icon_view):
        for tree_row in icon_view.get_model():
            if tree_row[ModelColumns.FULLPATH] == self._initial_path:
                icon_view.set_cursor(tree_row.path, None, False)
                break

    def _on_loaded(self):
        icon_view = self._enquiry("delta > icon view")
        if self._initial_path is not None:
            self._ensure_visble(icon_view)
        range_ = icon_view.get_visible_range()
        if range_ is None:
            return GLib.SOURCE_CONTINUE
        data = ModelSignals.REQUEST_PREVIEW, range_
        self._raise("delta > model signal", data)
        self._initial_path = None
        return GLib.SOURCE_REMOVE

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == ModelSignals.SET_INITIAL_PATH:
            self._initial_path = param
        if signal == ModelSignals.PARSING_FINISHED:
            GLib.timeout_add(10, self._on_loaded)

    def __init__(self, parent):
        self._parent = parent
        self._initial_path = None
        self._raise("delta > register model object", self)
