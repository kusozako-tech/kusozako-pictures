
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelSignals


class DeltaZoomByScroll(DeltaEntity):

    def _action(self, delta_y):
        current_scale = self._enquiry("delta > scale")
        if delta_y >= 0:
            new_scale = min(5, current_scale*1.05)
        elif 0 > delta_y:
            new_scale = max(0.05, current_scale*0.95)
        self._raise("delta > scale changed", new_scale)

    def receive_transmission(self, user_data):
        signal, delta_y = user_data
        if signal == ModelSignals.ZOOM_BY_SCROLL:
            self._action(delta_y)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register model object", self)
