
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_pictures import ModelSignals
from .ZoomButton import AlfaZoomButton


class DeltaZoomOriginalButton(AlfaZoomButton):

    SIGNAL = ModelSignals.ZOOM_ORIGINAL
    ICON_NAME = "zoom-original-symbolic"
    TOOLTIP_TEXT = _("Original Size")
