
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .RotateLeft import DeltaRotateLeft
from .RotateRight import DeltaRotateRight


class EchoRotateButtons:

    def __init__(self, parent):
        DeltaRotateLeft(parent)
        DeltaRotateRight(parent)
