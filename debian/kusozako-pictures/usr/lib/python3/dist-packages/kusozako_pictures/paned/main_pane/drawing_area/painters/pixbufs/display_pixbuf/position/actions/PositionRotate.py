
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelSignals


class DeltaPositionRotate(DeltaEntity):

    def _action(self, pixbufs):
        old_pixbuf, pixbuf = pixbufs
        origin_x, origin_y = self._enquiry("delta > origin")
        old_pixbuf_center_x = origin_x + old_pixbuf.get_width()/2
        old_pixbuf_center_y = origin_y + old_pixbuf.get_height()/2
        x = old_pixbuf_center_x - pixbuf.get_width()/2
        y = old_pixbuf_center_y - pixbuf.get_height()/2
        self._raise("delta > origin changed", (x, y))

    def receive_transmission(self, user_data):
        signal, pixbufs = user_data
        if signal == ModelSignals.POSITION_ROTATE:
            self._action(pixbufs)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register model object", self)
