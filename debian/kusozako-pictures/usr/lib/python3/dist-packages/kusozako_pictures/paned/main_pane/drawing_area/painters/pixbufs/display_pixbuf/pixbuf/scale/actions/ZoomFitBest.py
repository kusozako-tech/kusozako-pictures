
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelSignals


class DeltaZoomFitBest(DeltaEntity):

    def _action(self):
        pixbuf = self._enquiry("delta > source pixbuf")
        gdk_rectangle = self._enquiry("delta > allocated size")
        pixbuf_width = pixbuf.get_width()
        pixbuf_height = pixbuf.get_height()
        width_scale = min(1, gdk_rectangle.width/pixbuf_width)
        height_scale = min(1, gdk_rectangle.height/pixbuf_height)
        scale = min(width_scale, height_scale)
        self._raise("delta > scale changed fit best", scale)

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal == ModelSignals.ZOOM_FIT_BEST:
            self._action()

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register model object", self)
