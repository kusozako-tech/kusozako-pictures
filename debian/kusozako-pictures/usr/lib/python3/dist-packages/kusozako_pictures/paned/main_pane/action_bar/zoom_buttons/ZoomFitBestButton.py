
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_pictures import ModelSignals
from .ZoomButton import AlfaZoomButton


class DeltaZoomFitBestButton(AlfaZoomButton):

    SIGNAL = ModelSignals.ZOOM_FIT_BEST
    ICON_NAME = "zoom-fit-best-symbolic"
    TOOLTIP_TEXT = _("Fit")
