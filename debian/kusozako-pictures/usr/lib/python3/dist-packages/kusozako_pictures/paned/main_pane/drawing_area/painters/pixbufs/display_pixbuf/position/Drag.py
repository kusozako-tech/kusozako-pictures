
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelSignals


class DeltaDrag(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, offset = user_data
        if signal == ModelSignals.DRAG_UPDATE:
            self._offset = offset
            self._raise("delta > request queue draw")
        elif signal == ModelSignals.DRAG_END:
            self._raise("delta > drag end", self._offset)
            self._offset = (0, 0)

    def get_total_offset(self, origin):
        return origin[0]+self._offset[0], origin[1]+self._offset[1]

    def __init__(self, parent):
        self._parent = parent
        self._offset = (0, 0)
        self._raise("delta > register model object", self)
