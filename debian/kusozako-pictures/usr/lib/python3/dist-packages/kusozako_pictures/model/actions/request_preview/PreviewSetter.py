
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import cairo
from gi.repository import GLib
from gi.repository import Gdk
from gi.repository import GdkPixbuf
from libkusozako3 import WebP
from libkusozako3.Ux import Unit
from libkusozako3.thumbnail.Thumbnail import FoxtrotThumbnail
from libkusozako3.thumbnail import Type
from kusozako_pictures import ModelColumns
from kusozako_pictures import PreviewStatus

SIZE = Unit(16)
INTERP_TYPE = GdkPixbuf.InterpType.BILINEAR


class FoxtrotPreviewSetter:

    def _get_pixbuf(self, tree_row):
        path = tree_row[ModelColumns.FULLPATH]
        mime_type = tree_row[ModelColumns.MIME_TYPE]
        if mime_type == "image/webp":
            pixbuf = WebP.to_pixbuf(path)
        else:
            pixbuf = GdkPixbuf.Pixbuf.new_from_file(path)
        width = pixbuf.get_width()
        height = pixbuf.get_height()
        scale = min(SIZE/width, SIZE/height)
        # scale = max(SIZE/width, SIZE/height)
        new_width = int(width*scale)
        new_height = int(height*scale)
        return pixbuf.scale_simple(new_width, new_height, INTERP_TYPE)

    def _get_preview(self, tree_row):
        pixbuf, thumbnail_path = self._thumbnail.load_from_cache(
            tree_row[ModelColumns.FULLPATH],
            tree_row[ModelColumns.CHECKSUM]
            )
        if pixbuf is None:
            pixbuf = self._get_pixbuf(tree_row)
            pixbuf.savev(thumbnail_path, "png", [], [])
        surface = tree_row[ModelColumns.SURFACE]
        context = cairo.Context(surface)
        x = (SIZE-pixbuf.get_width())/2
        y = (SIZE-pixbuf.get_height())/2
        Gdk.cairo_set_source_pixbuf(context, pixbuf, x, y)
        context.paint()
        return Gdk.pixbuf_get_from_surface(surface, 0, 0, SIZE, SIZE)

    def _set_preview(self, tree_row):
        tree_row[ModelColumns.PREVIEW_STATUS] = PreviewStatus.PENDING
        tree_row[ModelColumns.THUMBNAIL] = self._get_preview(tree_row)

    def try_set_preview(self, tree_row):
        if tree_row[ModelColumns.PREVIEW_STATUS] != PreviewStatus.UNLOADED:
            return
        GLib.idle_add(self._set_preview, tree_row)

    def __init__(self):
        self._thumbnail = FoxtrotThumbnail(Type.NORMAL_CONTAIN)
