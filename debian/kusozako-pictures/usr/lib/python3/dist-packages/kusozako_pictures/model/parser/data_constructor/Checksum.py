
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib


class FoxtrotChecksum:

    def get_md5(self, fullpath):
        uri = GLib.filename_to_uri(fullpath, "")
        fullpath_by_bytes = bytes(uri, encoding="utf-8")
        self._checksum.update(fullpath_by_bytes)
        md5_checksum = self._checksum.get_string()
        self._checksum.reset()
        return md5_checksum

    def __init__(self):
        self._checksum = GLib.Checksum.new(GLib.ChecksumType.MD5)
