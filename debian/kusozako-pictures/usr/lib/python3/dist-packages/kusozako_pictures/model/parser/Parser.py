
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .data_constructor.DataConstructor import DeltaDataConstructor
from .FileEnumerator import DeltaFileEnumerator


class DeltaParser(DeltaEntity):

    def _delta_call_append_gio_file(self, user_data):
        self._data_constructor.construct(user_data)

    def parse(self, directory):
        self._file_enumerator.parse(directory)

    def __init__(self, parent):
        self._parent = parent
        self._data_constructor = DeltaDataConstructor(self)
        self._file_enumerator = DeltaFileEnumerator(self)
