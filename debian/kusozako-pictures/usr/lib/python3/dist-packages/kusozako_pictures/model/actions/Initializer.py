
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from libkusozako3.mime_type import MimeType
from kusozako_pictures import ModelSignals


class DeltaInitializer(DeltaEntity):

    def _get_path(self):
        files = self._enquiry("delta > command line files")
        for gio_file in files:
            path = gio_file.get_path()
            if MimeType.from_gio_file(gio_file) != "inode/directory":
                parent = gio_file.get_parent()
                return parent.get_path(), path
            else:
                return path, None
        return GLib.get_home_dir(), None

    def _action(self):
        directory, target = self._get_path()
        param = ModelSignals.SET_INITIAL_PATH, target
        self._raise("delta > model signal", param)
        self._raise("delta > request parse directory", directory)

    def receive_transmission(self, user_data):
        signal, signal_param = user_data
        if signal == ModelSignals.WIDGETS_REALIZED:
            self._action()

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register model object", self)
