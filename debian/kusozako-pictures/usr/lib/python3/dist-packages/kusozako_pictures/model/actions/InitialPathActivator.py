
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelSignals
from kusozako_pictures import ModelColumns


class DeltaInitialPathActivator(DeltaEntity):

    def _get_tree_row(self):
        model = self._enquiry("delta > model")
        for tree_row in model:
            if tree_row[ModelColumns.FULLPATH] == self._initial_path:
                return tree_row
        return model[0] if len(model) > 0 else None

    def _set_initial_path(self):
        tree_row = self._get_tree_row()
        if tree_row is not None:
            param = ModelSignals.PATH_ACTIVATED, tree_row
            self._raise("delta > model signal", param)

    def receive_transmission(self, user_data):
        signal, signal_param = user_data
        if signal == ModelSignals.SET_INITIAL_PATH:
            self._initial_path = signal_param
        if signal == ModelSignals.PARSING_FINISHED:
            self._set_initial_path()

    def __init__(self, parent):
        self._parent = parent
        self._initial_path = None
        self._raise("delta > register model object", self)
