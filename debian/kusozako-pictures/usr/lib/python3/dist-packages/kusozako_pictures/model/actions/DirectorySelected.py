
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelSignals


class DeltaDirectorySelected(DeltaEntity):

    def _action(self, directory):
        self._raise("delta > request parse directory", directory)

    def receive_transmission(self, user_data):
        signal, directory = user_data
        if signal == ModelSignals.DIRECTORY_SELECTED:
            self._action(directory)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register model object", self)
