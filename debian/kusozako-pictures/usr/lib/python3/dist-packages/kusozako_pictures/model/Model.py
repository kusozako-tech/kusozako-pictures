
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_pictures import ModelColumns
from kusozako_pictures import ModelSignals
from .parser.Parser import DeltaParser
from .actions.Actions import EchoActions


class DeltaModel(Gtk.ListStore, DeltaEntity):

    def _delta_call_append(self, row_data):
        self.append(row_data)

    def _delta_call_request_parse_directory(self, directory):
        self.clear()
        self._parser.parse(directory)

    def _delta_call_parsing_finished(self):
        param = ModelSignals.PARSING_FINISHED, None
        self._raise("delta > model signal", param)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ListStore.__init__(self, *ModelColumns.TYPES)
        self.set_sort_column_id(
            ModelColumns.COLLATE_KEY,
            Gtk.SortType.ASCENDING
            )
        self._parser = DeltaParser(self)
        EchoActions(self)
