
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from libkusozako3.mime_type import MimeType


class DeltaFileEnumerator(DeltaEntity):

    def _try_set_path(self, gio_file, file_info):
        mime_type = MimeType.from_file_info(gio_file, file_info)
        if not mime_type.startswith("image"):
            return
        user_data = gio_file, file_info, mime_type
        self._raise("delta > append gio file", user_data)

    def _next_files_async_finished(self, file_enumerator, task):
        files = file_enumerator.next_files_finish(task)
        if not files:
            self._raise("delta > parsing finished")
            return
        for file_info in files:
            gio_file = file_enumerator.get_child(file_info)
            self._try_set_path(gio_file, file_info)
        self._next_files_async(file_enumerator)

    def _next_files_async(self, file_enumerator):
        file_enumerator.next_files_async(
            16,
            GLib.PRIORITY_DEFAULT_IDLE,
            None,
            self._next_files_async_finished
            )

    def _on_enumerate_children_async_finished(self, source_object, task):
        file_enumerator = source_object.enumerate_children_finish(task)
        self._next_files_async(file_enumerator)

    def parse(self, path):
        gio_file = Gio.File.new_for_path(path)
        gio_file.enumerate_children_async(
            "*",
            Gio.FileQueryInfoFlags.NONE,
            GLib.PRIORITY_DEFAULT_IDLE,
            None,
            self._on_enumerate_children_async_finished
            )

    def __init__(self, parent):
        self._parent = parent
