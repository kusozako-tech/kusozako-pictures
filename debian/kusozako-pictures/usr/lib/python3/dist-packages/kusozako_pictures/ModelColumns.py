
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GObject
from gi.repository import Gio
from gi.repository import GdkPixbuf

FILE_INFO = 0
FULLPATH = 1
BASENAME = 2
CHECKSUM = 3
COLLATE_KEY = 4
SURFACE = 5
THUMBNAIL = 6
PREVIEW_STATUS = 7
MIME_TYPE = 8
NUMBER_OF_COLUMNS = 9

TYPES = (
    Gio.FileInfo,           # FILE_INFO
    str,                    # FULLPATH
    str,                    # BASEANME
    str,                    # MD5 Checksum
    str,                    # COLLATE KEY
    GObject.TYPE_PYOBJECT,  # cairo.Surface
    GdkPixbuf.Pixbuf,       # THUMBNAIL
    int,                    # must be 0 to 3, see PreviewStatus.py
    str,                    # mime type
)
